﻿using OpenTK;
using System;


namespace OpenTKTemplate
{
    public class Cube
    {
        public Vector2 CubeCoords; //Top-Left Corner of Cube
        public float CubeSize;//Width & height
        public Vector2 Direction;//Direction and speed

        public Cube(int screenWidth, int screenHeight)
        {
            CubeSize = 100+(float)(new Random().NextDouble() * 50);
            CubeCoords = new Vector2((float)new Random().NextDouble() * (screenWidth - CubeSize), (float)new Random().NextDouble() * (screenWidth - CubeSize));
            Direction = new Vector2((float)(new Random().NextDouble() - .5f) * 10, (float)(new Random().NextDouble() - .5f) * 10);
        }

        public void Move(int screenWidth, int screenHeight)
        {
            //Check Left side
            if((CubeCoords.X + Direction.X) <= 0) {
                Direction.X = -Direction.X;
            }
            //Check Top side
            if((CubeCoords.Y + Direction.Y) <= 0)
            {
                Direction.Y = -Direction.Y;
            }
            //Check Right side
            if((CubeCoords.X + Direction.X) >= screenWidth - CubeSize)
            {
                Direction.X = -Direction.X;
            }
            //Check Bottom side
            if((CubeCoords.Y + Direction.Y) >= screenHeight - CubeSize)
            {
                Direction.Y =  -Direction.Y;

            }

            CubeCoords.X += Direction.X;
            CubeCoords.Y += Direction.Y;
        }
        public float[] GetNormalizedVertices(int screenWidth, int screenHeight)
        {
            Tuple<float, float, float, float> edges = new Tuple<float, float, float, float>(
                -1 + 2*(CubeCoords.X / screenWidth) ,
                -1 + 2*CubeCoords.Y / screenHeight,
                -1 + 2*(CubeCoords.X + CubeSize) / screenWidth,
                -1 + 2*(CubeCoords.Y + CubeSize) / screenHeight
            );
            float[] retArr = new float[]
            {
                edges.Item1, edges.Item2, 0, //TL
                edges.Item3, edges.Item2, 0, //TR
                edges.Item3, edges.Item4, 0, //BR
                edges.Item1, edges.Item4, 0, //BL
            };

            return retArr;
        }
    }
}
