﻿#version 330 core
layout (location = 0) in vec3 aPosition;

out vec4 VertexColor;

float normalizeToColor(float value){
	//Value Range [-1;1] .. Return Value Range [0;1]
	return (value+1)/2;
}
vec4 colorFromXYZ(vec3 pos){
	return vec4(normalizeToColor(pos.x),normalizeToColor(pos.y),normalizeToColor(pos.z),1.0);
	
}

void main(void){
	gl_Position = vec4((aPosition),1.0);
	
	VertexColor = colorFromXYZ(aPosition);
	
}

