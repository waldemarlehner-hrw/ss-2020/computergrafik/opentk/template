﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;
using System;

namespace OpenTKTemplate
{
    internal class Program : GameWindow
    {
        private Shader shader;
        private Cube cube;
        private int VertexBufferObject, VertexArrayObject;

        public Program() : base(800, 600, GraphicsMode.Default, "Test")
        {
            VSync = VSyncMode.On;
        }

        protected override void OnLoad(EventArgs e)
        {
            GL.ClearColor(.5f, .5f, .5f, 1.0f);
        
            VertexBufferObject = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferObject);

            //Spawn cube
            cube = new Cube(Width, Height);
            var vertices = cube.GetNormalizedVertices(Width, Height);
            //var vertices = new float[] { -.5f, -.5f, 0f, 0f, .5f, 0f, .5f, -.5f, 0f };
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);

            //Add shader
            shader = new Shader("Shaders/shader.vert", "Shaders/shader.frag");
            shader.Use();

            VertexArrayObject = GL.GenVertexArray();
            GL.BindVertexArray(VertexArrayObject);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 3 * sizeof(float), 0);

            GL.EnableVertexAttribArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferObject);

            base.OnLoad(e);
        }

        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, Width, Height);
            base.OnResize(e);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            KeyboardState keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Key.Escape))
                Exit();

            GL.Clear(ClearBufferMask.ColorBufferBit);
            cube.Move(Width, Height);

            base.OnUpdateFrame(e);
        }

        protected override void OnUnload(EventArgs e)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.DeleteBuffer(VertexBufferObject);
            shader.Dispose();

            base.OnUnload(e);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            var vertices = cube.GetNormalizedVertices(Width, Height);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StreamDraw);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, 3 * sizeof(float), 0);
            GL.EnableVertexAttribArray(0);

            shader.Use();
            //Get Vertex Buffer
            GL.BindVertexArray(VertexArrayObject);
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferObject);
            //Update Buffer with new vertices
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StreamDraw);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 3 * sizeof(float), 0);
            GL.EnableVertexAttribArray(0);
            GL.BindVertexArray(VertexArrayObject);

            GL.DrawArrays(PrimitiveType.Quads, 0, 4);

            SwapBuffers();
            base.OnRenderFrame(e);
        }

        [STAThread]
        private static void Main()
        {
            using (Program program = new Program())
            {
                program.VSync = VSyncMode.Adaptive;
                program.Run(60.0f, 0.0f);
            }
        }
    }
}